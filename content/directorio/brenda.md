+++
name = "Brenda Laura Vázquez Hernández"
nickname = "pentcode"
description = "Lic. Redes y Servicios de Cómputo, Programador Jr."
skills = "java, php, css, javascript, linux, redes, IA"
thumbnail = "/img/directorio/brenda.jpg"
draft = false
date = "2019-01-14T19:37:18-06:00"
disponibility = "Solo Remoto"
status = "Ocupada"
rol = "admin"
website = "https://gitlab.com/pentcode/"
author = "Brenda"
+++

Me gustan las redes de computadoras, tirar codiguito y los temas relacionados con IA.
